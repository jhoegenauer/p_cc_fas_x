//===========================================================
// map.js - main module for map
//
//-----------------------------
// TO DO:
// ...add items...
//
//-----------------------------
// BUGS:
// ...add items...

// !!! CWIS:
// looks like last update only updated when data points changed
// look at api
// "on update" should trigger whether or not any sites require updating with new data ???

//
//===========================================================

// create module object
if ( window.main === undefined) { window.main = {}; }

//===========================================================
// PROPERTIES

// AGS url
main.AGSurl = "http://txpub.usgs.gov/arcgis/rest/services/FAS/FAS_RioGrande/MapServer";

// list of HUC8's to spatially limit CWIS queries to
main.CWIS_HUC8s = "13010001,13010002,13010003,13010004,13010005,13020101,13020102,13020201,13020202,13020203,13020204,13020205,13020206,13020207,13020208,13020209,13020210,13020211,13030101,13030102,13030103,13040100"; // dpearson 20160928


//===========================================================
// FUNCTIONS

//-----------------------------------------------------------
// init
//   startup function
main.init = function () {
    var funcName = "main [init]: ";
    
    // make map
    require(
        [
            "esri/geometry/Extent", "esri/SpatialReference", // study area extent
            "esri/map", "esri/dijit/HomeButton", "esri/dijit/Scalebar", "esri/geometry/webMercatorUtils", // map and widgets
            "esri/layers/ArcGISTiledMapServiceLayer", "esri/layers/ArcGISDynamicMapServiceLayer", // layers
            "esri/geometry/Point", // search_api
            "dojo/domReady!"
        ], function
        (
            Extent, SpatialReference, // study area
            Map, HomeButton, Scalebar, webMercatorUtils, // map and widgets
            ArcGISTiledMapServiceLayer, ArcGISDynamicMapServiceLayer, // layers
            Point // search_api
        ) {
        
        //......................................
        // esri config: click-drag zoom box style
        esriConfig.defaults.map.zoomSymbol = {
            "style" : "esriSFSSolid",
            "color"   : [0,170,230,30],
            "outline" : {
                "style" : "esriSLSDot",
                "color" : [0,170,230,255],
                "width" : 2
            }
        };
        
        // set study area extent
        main.studyExtent = new Extent( -108.52747, 30.23587, -105.12067, 38.46137, new SpatialReference({wkid:4326}));
        
        //......................................
        // create map
        console.log(funcName + "creating map");
        main.mapSelector = $("#map");
        main.map = new Map( "map", {
            "autoResize"           : true,             // have map automatically resize when browser window / map ContentPane resizes?
            "displayGraphicsOnPan" : true,             // true = graphics are displayed during panning (false can improve performance in IE)
            "extent"               : main.studyExtent, // startup extent
            "fitExtent"            : true,             // true = guaranteed to have the initial extent shown completely on the map
            "lods" : [
              // make sure all basemaps used are befined at all zoom levels allowed below
              //{ 'level' :  0, 'resolution' : 156543.033928000000000, 'scale' : 591657527.591555 }, // farthest out possible
              //{ 'level' :  1, 'resolution' :  78271.516963999900000, 'scale' : 295828763.795777 },
              //{ 'level' :  2, 'resolution' :  39135.758482000100000, 'scale' : 147914381.897889 },
              //{ 'level' :  3, 'resolution' :  19567.879240999900000, 'scale' :  73957190.948944 },
              //{ 'level' :  4, 'resolution' :   9783.939620499960000, 'scale' :  36978595.474472 },
                { 'level' :  5, 'resolution' :   4891.969810249980000, 'scale' :  18489297.737236 }, //  0 <== farthest out allowed
                { 'level' :  6, 'resolution' :   2445.984905124990000, 'scale' :   9244648.868618 }, //  1
                { 'level' :  7, 'resolution' :   1222.992452562490000, 'scale' :   4622324.434309 }, //  2
                { 'level' :  8, 'resolution' :    611.496226281380000, 'scale' :   2311162.217155 }, //  3
                { 'level' :  9, 'resolution' :    305.748113140558000, 'scale' :   1155581.108577 }, //  4
                { 'level' : 10, 'resolution' :    152.874056570411000, 'scale' :    577790.554289 }, //  5
                { 'level' : 11, 'resolution' :     76.437028285073200, 'scale' :    288895.277144 }, //  6
                { 'level' : 12, 'resolution' :     38.218514142536600, 'scale' :    144447.638572 }, //  7
                { 'level' : 13, 'resolution' :     19.109257071268300, 'scale' :     72223.819286 }, //  8
                { 'level' : 14, 'resolution' :      9.554628535634150, 'scale' :     36111.909643 }, //  9
                { 'level' : 15, 'resolution' :      4.777314267949370, 'scale' :     18055.954822 }  // 10 <== farthest in allowed (CWIS graphic points disappear closer in)
              //{ 'level' : 16, 'resolution' :      2.388657133974680, 'scale' :      9027.977411 }
              //{ 'level' : 17, 'resolution' :      1.194328566855050, 'scale' :      4513.988705 }
              //{ 'level' : 18, 'resolution' :      0.597164283559817, 'scale' :      2256.994353 }
              //{ 'level' : 19, 'resolution' :      0.298582141647617, 'scale' :      1128.497176 }  // farthest in possible
            ],
            "logo"                  : false,     // display logo on map?
            "nav"                   : false,     // display pan buttons on map?
            "optimizePanAnimation"  : false,     // skip panning animation when calling map.centerAt() or map.setExtent() ?
            "resizeDelay"           : 300,       // time [millisec] to ignore repeated calls to the resize method
            "showAttribution"       : false,     // enable or disable map attribution in lower right corner of map
            "showInfoWindowOnClick" : true,      // show InfoWindow if Graphic has a defined InfoTemplate when the user clicks the graphic
            "slider"                : true,      // display zoom slider on map?
            "wrapAround180"         : true       // true = continuous pan across dateline; supported in the following cases (see API ref for caveats)
        });
        
        // add basemaps (triggers map on load) and thumb picker
        main.map.addLayer( new ArcGISTiledMapServiceLayer( "http://services.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/",                         {"id":"BASEimagery1", "opacity":0.8, "visible":true  }) ); // imagery base (no labels)
        main.map.addLayer( new ArcGISTiledMapServiceLayer( "http://services.arcgisonline.com/ArcGIS/rest/services/Reference/World_Boundaries_and_Places/MapServer/", {"id":"BASEimagery2", "opacity":1.0, "visible":true  }) ); // imagery labels
        main.map.addLayer( new ArcGISTiledMapServiceLayer( "http://services.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/",                      {"id":"BASEstreets",  "opacity":1.0, "visible":false }) ); // streets with labels
        main.mapSelector.append('<div id="BasemapThumb" data-new-basemap="streets" title="View streets base map"></div>');
        $("#BasemapThumb").on("click", function() {
            $.each( ["BASEstreets","BASEimagery1","BASEimagery2"], function(idx,layer_id) {
                main.map.getLayer(layer_id).setVisibility( !main.map.getLayer(layer_id).visible ); // toggle basemap layer visibility
            });
            var new_basemap = ( $(this).attr("data-new-basemap")==="streets" ? "imagery" : "streets" ); // toggle new basemap
            $(this)
                .attr( "data-new-basemap", new_basemap )           // update data (updates thumb)
                .prop( "title", "View "+new_basemap+" base map" ); // update hover title
            main.map.getLayer("AGSmask").setOpacity( new_basemap==="streets" ? 0.6 : 0.3 ); // adjust mask opacity (darker for imagery)
        });
        
        //......................................
        // finalize map on load
        main.map.on("load", function() {
            
            //................
            // add widgets
            
            // home button
            main.mapSelector.append('<div id="HomeButton"></div>');
            new HomeButton({
                "map"    : main.map,
                "extent" : main.studyExtent
            }, $("#HomeButton")[0] );
            
            // scalebar
            main.mapSelector.append('<div id="Scalebar" class="map-widget-container"></div>');
            new Scalebar({
                "map"          : main.map,
                "scalebarUnit" : "dual"
            }, $("#Scalebar")[0] );
            
            // lat-lon mouse coords
            main.mapSelector.append('<div id="LatLon" class="map-widget-container"></div>');
            $("#LatLon").html( "Scale 1:" + main.numAddCommas(main.map.getScale().toFixed(0)) ); // init startup
            
            // legend
            main.mapSelector.append('<div id="Legend"></div>');
            
            // last update
            main.mapSelector.append('<div id="LastUpdate" class="map-widget-container h-center-me"></div>');
            main.center_mes();
            
            //................
            // add layers
            
            // AGS layers
            main.map.addLayer( new ArcGISDynamicMapServiceLayer( main.AGSurl, { "id":"AGSmask",   "opacity":0.6, "visible":true }) );  main.map.getLayer("AGSmask"  ).setVisibleLayers([2]); // Rio Grande Study Area Mask (2)
            main.map.addLayer( new ArcGISDynamicMapServiceLayer( main.AGSurl, { "id":"AGSbasins", "opacity":1.0, "visible":true }) );  main.map.getLayer("AGSbasins").setVisibleLayers([0]); // Sub-basins HUC8 (0)
            
            // CWIS layers
            CWIS._caller = "RioGrandeFAS"; // database logging tag
            CWIS.verbose = true;           // turn debug logging on
            CWIS.addLayer(
                "flow",
                { // service opts
                    "HUC8s"       : main.CWIS_HUC8s, // study area made up of these HUC8s
                    "outSiteInfo" : "summary"        // extended site info in popup
                },
                { // layer opts
                    "map"          : main.map,
                    "layer_id"     : "CWISflow",
                    "labels"       : true,
                    "labels_scale" : 300000,
                    "on_success"   : function() { $("#LastUpdate").html( "Real-time data is latest available as of<br/>" + main.formatDate(new Date()) ); },
                    "on_update"    : function() { $("#LastUpdate").html( "Real-time data is latest available as of<br/>" + main.formatDate(new Date()) ); }
                }
            );
            
            //................
            // misc
            
            // no mouse text click-drag on map children after all map elements created
            main.mapSelector.find("*").addClass("click-drag-unselectable");
            
            // size map height to browser height so can see full map
            main.resizeLayout = function() {
                $("#map_container,#map").height( 0.9*$(window).innerHeight() );
            }
            $(window).resize( main.resizeLayout );
            main.resizeLayout();
            
            // zoom to study area, fade out and remove loading
            setTimeout( function() {
                main.map.setExtent( main.studyExtent, true );
                $("#map_loading").fadeOut( 1000, function(){
                    $(this).remove();
                });
            }, 1000);
            
        }); // end map on load
        
        //......................................
        // map events
        
        // update scale-lat-lon text on map when mouse moves over map
        main.map.on("mouse-move,mouse-drag", function (evt) {
            var mp = webMercatorUtils.webMercatorToGeographic(evt.mapPoint);
            $("#LatLon").html( "Scale 1:" + main.numAddCommas(main.map.getScale().toFixed(0)) + "&nbsp;&nbsp;&nbsp;(" + mp.y.toFixed(5) + ", " + mp.x.toFixed(5) + ")");
        });
        
        // update scale when mouse exits map and zoom changes
        main.map.on("mouse-out,zoom-end",  function () {
            $("#LatLon").html( "Scale 1:" + main.numAddCommas(main.map.getScale().toFixed(0)) );
        });
        
        // do not allow navigation outside study extent
        main.lastOkExtent = main.studyExtent;
        main.map.on("extent-change", function () {
            if ( ! main.map.extent.intersects(main.studyExtent) )  {
                // current map extent outside study extent
                console.log("extent-change callback: navigated outside study area - going to last good extent");
                main.map.setExtent(main.lastOkExtent);
            } else {
                // extent OK - reset lastGood
                main.lastOkExtent = main.map.extent;
            }
        });
        
        //......................................
        // search_api
        search_api.on("load", function() {
            
            // move textbox into map and show
            $("#sapi_searchTextBoxDiv").appendTo( main.mapSelector ).show();
            
            // set caller
            search_api._p.caller = "RioGrandeFAS";
            
            // set search api options (case-sensitive)
            search_api.setOpts({
                // ...searchable area...
                "LATmin" : main.studyExtent.ymin,
                "LATmax" : main.studyExtent.ymax,
                "LONmin" : main.studyExtent.xmin,
                "LONmax" : main.studyExtent.xmax,
                // ...database search options...
                "DbSearchUseCommonGnisClasses" : false, // use all (major and minor) GNIS location categories
                "DbSearchIncludeUsgsSiteSW"    : true,  // include USGS SW gages
                // ...autocomplete menu options...
                "menuMaxEntries"         : 100,
                "autoCompleteTimeoutSec" : 5,
                // ...textbox options...
                "textboxPosition"      : "top-left",
                "textboxOffsetXpx"     : 50,
                "textboxOffsetYpx"     : 10,
                "textboxHoverString"   : "Search for a location in and around the study area",
                "textboxSearchState"   : "shadeGray",
                "enterSearchMinLength" : 3
            });
            
            // OPTIONAL: define what to do when a search is started
            search_api.on("before-search", function() {
                main.map.infoWindow.hide(); // close any infoWindows
                setTimeout( function() { // set wait mouse cursor if a location not found in 1/2 second
                    if (search_api.isSearching === true) { main.map.setMapCursor("wait"); }
                }, 500);
            });
            
            // REQUIRED: define what to do when a search result is returned
            search_api.on("location-found", function(props) {
                // set popup content
                main.map.infoWindow.setTitle("Search Result (" + props.pctMatch.toFixed(0) + "% Match)" );
                main.map.infoWindow.setContent(
                    '<div class="cwis-popup">' +
                        '<h1>' + props.label  + '</h1>' +
                        '<table>' +
                            '<tr><td colspan="2" class="cwis-table-header1">Location Details</td></tr>' +
                            '<tr><td>Name     </td><td>' + props.name     + '</td></tr>' +
                            '<tr><td>Category </td><td>' + props.category + '</td></tr>' +
                            '<tr><td>County   </td><td>' + props.county   + '</td></tr>' +
                            '<tr><td>State    </td><td>' + props.state    + '</td></tr>' +
                            '<tr><td colspan="2" class="cwis-table-header1">Location Coordinates</td></tr>' +
                            '<tr><td>Latitude </td><td>' + props.y        + '</td></tr>' +
                            '<tr><td>Longitude</td><td>' + props.x        + '</td></tr>' +
                            '<tr><td>Elevation</td><td>' + props.elevFt   + '</td></tr>' +
                        '</table>' +
                    '</div>'
                );
                main.map.infoWindow.features = []; // clear previous popup features
                // show popup and zoom to point
                var pt = new Point( props.x, props.y, new SpatialReference({"wkid":4326}) );
                main.map.infoWindow.show( pt );
                main.map.centerAndZoom( pt, Math.round( main.map.getMaxZoom()/2 ) ); // center and zoom to 1/2-level
                main.map.setMapCursor("default");
            });
            
            // OPTIONAL: define what to do when no location is found
            search_api.on("no-result", function() {
                main.map.setMapCursor("default");
                alert('A location for "' + search_api.textboxGetString() + '" could not be found within the study area.');
            });
            
            // OPTIONAL: define what to do when a search times out
            search_api.on("timeout", function() {
                main.map.setMapCursor("default");
                alert('A location for "' + search_api.textboxGetString() + '" could not be found within the study area.');
            });
            
        }); // end search_api on "load"
        
    }); // end require
    
}; // init


//-----------------------------------------------------------
// numAddCommas
//   add commas to an integer (no decimals)
main.numAddCommas = function ( intgr ) {
    
    // regex replace (doesn't handle decimal part correctly)
    while ( /(\d+)(\d{3})/.test(intgr.toString()) ) {
        intgr = intgr.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
    }
    return intgr;
    
}; // numAddCommas


//-----------------------------------------------------------
// formatDate
//   returns formatted date-time string for input Date object:
//   "Thu Apr 10 2014, 4:08 PM"
main.formatDate = function ( dateObj ) {
    
    // get date part
    var dateStr = dateObj.toDateString();
    
    // HH
    var HH = dateObj.getHours(); // hour (from 0-23)
    var AmPm = "AM";
    if (HH >= 12) { AmPm = "PM";    }
    if (HH >  12) { HH   = HH - 12; }
    HH = HH.toString();
    
    // MM
    var MM = dateObj.getMinutes(); // minutes (from 0-59)
    MM = MM.toString();
    if (MM.length < 2) { MM = "0"+MM; }
    
    // return foramtted date & time
    return dateStr + ", " + HH + ":" + MM + " " + AmPm;
    
}; // formatDate


//-----------------------------------------------------------
// center_mes
//   center things that have center-me classes:
//   ".h-center-me" center horizontally only
//   ".v-center-me" center vertically only
//     ".center-me" center both vertically and horizontally
// centering is done relative their parent
// IMPORTANT: will NOT center HIDDEN things
main.center_mes = function () {
    
    // horizontal centering
    $(".h-center-me, .center-me").each( function() {
        $(this).css({ "margin-left" : ( $(this).parent().width() - $(this).width() )/2 + "px" });
    });
    // vertical centering
    $(".v-center-me, .center-me").each( function() {
        $(this).css({ "margin-top" : ( $(this).parent().height() - $(this).height() )/2 + "px" });
    });
    
}; // center_mes
$(window).resize( main.center_mes ); // execute on window resize


//-----------------------------------------------------------
// ...more functions...

// //-----------------------------------------------------------
// // XXX
// //   XXX
// main.XXX = function() {
    // var funcName = "main [XXX]: ";
    // console.log(funcName + "");
    
    // // XXX
    
// }; // XXX


//===========================================================
// END
