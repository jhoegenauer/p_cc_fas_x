//===========================================================
// util_ui.js - user interface utility module
//
// JS DEPENDENCIES:
// * jquery
// * bootstrap 3.3.7
//
// DATA DEPENDENCIES
// * none
//
// written by: Joe Vrabel, USGS TX-WSC Austin (jvrabel@usgs.gov)
// uses modified open source plugins from other authors (original sources noted in un-minified code where applicable)
//===========================================================
//"use strict";

// check dependencies
if (window.jQuery===undefined) {
    console.warn("[util_ui] jQuery must be loaded - util_ui may not function correctly");
}
if (typeof $().modal!=="function") {
    console.warn("[util_ui] bootstrap must be loaded - util_ui may not function correctly");
}

// create module object - all utils use same object
if (util === undefined) { var util = {}; }

//===========================================================
// PROPERTIES
//===========================================================

// (none)


//===========================================================
// FUNCTIONS
//===========================================================

//-----------------------------------------------------------
// createPanel
//     create expandable panel
// input:
//     bid      - (required) button  element, "#id" or $(selector)
//     pid      - (required) panel   element, "#id" or $(selector)
//     did      - (required) docking element, "#id" or $(selector)
//     position - (optional) one of:
//                   "down-right": open panel downward, right justify with bid (default)
//                   "down-left" : open panel downward, left  justify with bid
//                   "up-right"  : open panel upward,   right justify with bid
//                   "up-left"   : open panel upward,   left  justify with bid
util.createPanel = function ( bid, pid, did, position ) {
    
    // make sure elements exist
    if ( $(bid).length<=0 || $(pid).length<=0 || $(did).length<=0 ) {
        console.warn("util_ui [createPanel]: input element(s) do not exist in document");
        return;
    }

    // set default position
    if (position === undefined) { position = "down-left"; }
    
    // setup panel
    $(pid)
        .addClass("util-ui-panel util-ui-panel-" + position + " util-ui-panel-hidden")
        .appendTo( $(did).parent() )   // move to dock parent for positioning
        .stop().fadeOut(0)             // hide
        .css({ visibility:"visible" }) // make visible
        .mouseenter( function() {
            $(".util-ui-panel").css("z-index","81"); // put other panels behind
            $(this).css("z-index","82");             // put this panel on top
        });
    
    // function to position panel
    var positionPanel = function() {
        if ( $(did).length<=0 || $(pid).length<=0 ) { return; }
        var left = $(did).position().left;
        var top  = $(did).position().top;
        switch(position) {
            case "up-right":
                left = left + $(did).outerWidth() - $(pid).outerWidth();
                top  = top  - $(pid).outerHeight();
                break;
            case "up-left":
                top  = top  - $(pid).outerHeight();
                break;
            case "down-right":
                left = left + $(did).outerWidth() - $(pid).outerWidth();
                top  = top  + $(did).outerHeight();
                break;
            case "down-left":
            default:
                top  = top  + $(did).outerHeight();
                break;
        }
        $(pid).css({
            left : left + "px",
            top  :  top + "px"
        });
    };

    // position on creation and resize
    positionPanel();
    $(window).resize(function() {
        positionPanel();
    });
    
    // clicking button opens or closes panel
    $(bid).click( function() {
        positionPanel();
        util.togglePanel(pid);
    });
    
}; // createPanel


//-----------------------------------------------------------
// togglePanel
//     show or hide panel
// input:
//     pid  - (required) panel   element, "#id" or $(selector)
//     show - (optional) whether to show (true) or hide (false)
//            if omitted, panel is toggled (shown if hidden and vice-versa)
util.togglePanel = function( pid, show ) {
    
    // toggle if show not input
    var isShown = (! $(pid).hasClass("util-ui-panel-hidden"));
    if (show === undefined) { show = !isShown; }
    
    // if opens up, do not slide
    var slideMs = 200;
    if ( $(pid).is(".util-ui-panel-up-left") || $(pid).is(".util-ui-panel-up-right") ) { slideMs = 0; }
    
    // show or hide
    $(".util-ui-panel").css("z-index","81");
    if (show) {
        // show
        if (isShown) { return; }
        $(pid)
            .stop()
            .fadeIn(   { duration:200,     queue:false }).css({ display:"none" })
            .slideDown({ duration:slideMs, queue:false, always:function(){ $(this).css("height","auto") } })
            .removeClass("util-ui-panel-hidden")
            .css({ "z-index":"82" });
        
    } else {
        // hide
        if (!isShown) { return; }
        $(pid)
            .stop()
            .fadeOut({ duration:200,     queue:false })
            .slideUp({ duration:slideMs, queue:false })
            .addClass("util-ui-panel-hidden");
    }
    
}; // togglePanel


//-----------------------------------------------------------
// alert
//     display a bootstrap modal dialog containing a message with options
//
// input:
//     msg (optional, string or array)
//         dialog html content, can be string or array of strings
//         when array, each element is wrapped in a <p>
//         omit or set false to close any open alert
//         default: false (closes any open, does nothing if none open)
//
//     opts (optional) options object with these properties:
//         opts.title - (optional, string or bool)
//             title to display in title bar
//             omit or set false to not show title bar
//             default: false (title bar not shown)
//
//         opts.allowNoShow - (optional, bool)
//             whether to include a checkbox that gives user the option not to show this dialog again
//             when user checks the box, calling this function again with the exact same "msg" will do nothing
//             default: false
//
//         opts.allowClose - (optional, bool)
//             whether to allow the dialog to be closed (eg: no close button, no close "X", clicking overlay does not close)
//             when set false you need to write your own code to close the dialog
//             default: true
//
//         opts.showOnce - (optional, bool)
//             whether to only show once (similar to allowNoShow but without checkbox option)
//             calling this function again with the exact same "msg" will do nothing
//             if true, allowNoShow checkbox is never shown
//             default: false
//
//         opts.width - (optional, number)
//             dialog width, in pixels
//             90% of the window width is used if this value is larger than the window width
//             default: 600px
//
//         opts.center - (optional, bool)
//             whether to center the dialog vertically on the screen
//             dialog is always centered horizontally
//             always fades into view (does not slide down) when centered vertically
//             default: false (position at top of screen)
//
//         opts.footer - (optional, string or bool false)
//             html string for additional things to add to left of the footer "close" button, for example additional buttons
//             set false to not show the footer
//             default: nothing added to footer
//             example: '<button type="button" class="btn btn-sm btn-primary" onclick="alert(\'do something\');">Do Something</button>'
//             example: false // footer not shown
//
//         opts.slideDown - (optional, bool)
//             whether to slide the dialog into view from the top of the page (true) or just have it fade in (false)
//             default: true
//
util.alert = function(msg,opts) {
    var funcName = "util [alert]: ";
    
    // close any open if no message
    if( !msg ){
        $("#util_ui_alert").modal("hide");
        return;
    }
    
    // set defaults
    if( opts             === undefined ){ opts             = {};    }
    if( opts.title       === undefined ){ opts.title       = false; }
    if( opts.allowNoShow === undefined ){ opts.allowNoShow = false; }
    if( opts.allowClose  === undefined ){ opts.allowClose  = true;  }
    if( opts.showOnce    === undefined ){ opts.showOnce    = false; }
    if( opts.width       === undefined ){ opts.width       = 600;   }
    if( opts.center      === undefined ){ opts.center      = false; }
    if( opts.footer      === undefined ){ opts.footer      = "";    }
    if( opts.slideDown   === undefined ){ opts.slideDown   = true;  }
    if( opts.center ){ opts.slideDown = false; }
    
    // convert msg array to string if needed
    if ( typeof msg!=="string" ) { msg = "<p>"+ msg.join("</p><p>") +"</p>"; }
    
    // simple function to hash a string
    var hashCode = function(s) {
        var hash = 0;
        if (s.length===0) return hash;
        for (var i=0; i<s.length; i++) {
            var c = s.charCodeAt(i);
            hash = ((hash<<5)-hash)+c;
            hash = hash & hash; // convert to int32
        }
        return hash;
    };
    
    // use hashed msg to assign "unique" id for this alert
    var alertID = hashCode(msg);
    
    // do not show if already shown and:
    // - allowNoShow true and user checked box for this msg
    // - showOnce    true
    if (util.alert.alertsShown === undefined) { util.alert.alertsShown = {}; }
    if ( (opts.allowNoShow===true || opts.showOnce===true) && util.alert.alertsShown[alertID]===true ) {
        console.log(funcName+"alert with this message already shown and user opted not to show again - doing nothing");
        return;
    }
    if (opts.showOnce===true) {
        util.alert.alertsShown[alertID] = true; // do not show again
        opts.allowNoShow = false;               // don't show no-show checkbox even if specified
    }
    
    // add modal to doc
    $("#util_ui_alert, .modal-backdrop").remove();
    $("body").append('\
        <div id="util_ui_alert" class="modal '+( opts.slideDown ? "fade":"" )+'" role="dialog"> \
            <div style="display:none;" class="modal-dialog util-ui-center-me-h '+( opts.center ? "util-ui-center-me-v":"" )+'" data-width="'+ opts.width +'"> \
                <div class="modal-content"> \
                    <div class="modal-header '+( opts.title ? "":"hidden" )+'"> \
                        '+( opts.allowClose ? '<button type="button" class="close" data-dismiss="modal">&times;</button>' : '' )+'\
                        <h4 class="modal-title">'+ opts.title +'</h4> \
                    </div> \
                    <div class="modal-body">'+ msg +'</div> \
                    <div class="modal-footer '+( opts.footer===false ? "hidden":"" )+'"> \
                        <div class="checkbox '+( opts.allowNoShow ? "":"hidden" )+'" style="float:left;"> \
                            <label><input type="checkbox" value="">Do not show this again</label> \
                        </div> \
                        <div style="float:right;"> \
                            <span>'+ opts.footer +'</span>&nbsp; \
                            '+( opts.allowClose ? '<button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>' : '' )+'\
                        </div> \
                    </div> \
                </div> \
            </div> \
        </div> \
    ');
    
    // save setting when no show checkbox checkstate changes
    $("#util_ui_alert .modal-footer input:first")
        .prop("checked",false)
        .off("change")
        .on( "change", function() {
            util.alert.alertsShown[alertID] = $(this).prop("checked");
        });
    
    // show dialog
    $("#util_ui_alert").modal({
        backdrop: ( opts.allowClose ? true  : "static" ),
        keyboard: ( opts.allowClose ? false : true     )
    });
    $("#util_ui_alert .modal-dialog").fadeIn(400);
    $(window).resize();
    util.centerMes();
    
}; // alert
$( window ).resize(function() {
    $("#util_ui_alert .modal-dialog").css({
        width: Math.min(
            $("#util_ui_alert .modal-dialog").data("width"),
            0.9*$(window).width()
        )+"px"
    });
    $("#util_ui_alert .modal-body").css({
        "max-height" : 0.8*$(window).height(),
        "overflow"   : "auto"
    });
});


//-----------------------------------------------------------
// popupWindow
//     launch 'url' in browser popup window with options
//     'opts' can have the following properties:
//        - width   (integer) : width  of popup, in pixels (default 50% screen width )
//        - height  (integer) : height of popup, in pixels (default 70% screen height)
//        - left    (integer) : left position in the screen, in pixels (default centered in screen)
//        - top     (integer) : top  position in the screen, in pixels (default centered in screen)
//        - noCache (boolean) : whether to add timestamp to url to prevent browser caching (default false)
//     returns the window opened
util.popupWindow = function( url,opts ) {
    
    // set opt defaults
    if (opts        === undefined) { opts = {}; }
    if (opts.width  === undefined) { opts.width  = 0.5 * window.screen.width;  }
    if (opts.height === undefined) { opts.height = 0.7 * window.screen.height; }
    if (opts.left   === undefined) { opts.left = (window.screen.width /2) - (opts.width / 2); }
    if (opts.top    === undefined) { opts.top  = (window.screen.height/2) - (opts.height/ 2); }
    
    // adjust for multi-screen
    opts.left += (window.screenLeft ? window.screenLeft : window.screenX);
    opts.top  += (window.screenTop  ? window.screenTop  : window.screenY);
    
    // add time-stamp to url if noCache
    if (opts.noCache === true) {
        url += ( url.match(/\?/) ? "&_t=" : "?_t="  ) + (+new Date);
    }
    
    // popup the window
    var win = window.open( url, "_blank",
        // ...options - not all browsers recognize...
        "toolbar=no,"     +
        "location=no,"    +
        "directories=no," +
        "status=no,"      +
        "menubar=no,"     +
        "scrollbars=yes," +
        "resizable=yes,"  +
        "copyhistory=no," +
        "width="          + Math.floor( opts.width  ) + "," +
        "height="         + Math.floor( opts.height ) + "," +
        "left="           + Math.floor( opts.left   ) + "," +
        "top="            + Math.floor( opts.top    )
    );
    win.focus();
    return win;
    
}; // popupWindow


//-----------------------------------------------------------
// enableTooltips
//     enable or disable bootstrap tooltips for entire document
//     tooltips are off at startup until this function is called to turn them on
// input:
//     enable - (optional) true = enable, false = disable; toggled if omitted
util.enableTooltips = function ( enable ) {
    var funcName = "util [enableTooltips]: ";
    if ( enable===undefined ) { enable = ( $("body").is(".util-ui-tooltips-enabled") ? false : true ); }
    console.log(funcName + "enable " + enable);
    if (enable) {
        $("*[title]").tooltip({
            animation : true,
            container : "body",
            delay     : 500,
            html      : true,
            placement : "auto",
            trigger   : "hover"
        });
        $("body").addClass("util-ui-tooltips-enabled");
    } else {
        $("*[title]").tooltip("destroy");
        $("body").removeClass("util-ui-tooltips-enabled");
    }
}; // enableTooltips


//-----------------------------------------------------------
// enableDragSelect
//     enable or disable mouse-drag-select on element and children
//     when disabled, click-drag on text does not highlight
// input:
//     enable (REQUIRED)
//         whether to enable (true) or disable (false)
//     parent (optional)
//         parent element to use, either:
//         * id with "#", example: "#myElemId"
//         * jQuery selector, example: $("div")
//         parent element set to entire document if omitted
util.enableDragSelect = function ( enable, parent ) {
    if (parent===undefined) {
        parent = $("body");
    } else if ( typeof parent === "string" ) {
        parent = $(parent);
    }
    if ( enable ) {
        parent.removeClass("util-ui-unselectable");
    } else {
        parent.addClass(   "util-ui-unselectable");
    }
}; // enableDragSelect


//-----------------------------------------------------------
// centerMes
//     center things that have center-me classes:
//         ".util-ui-center-me-h" center horizontally only
//         ".util-ui-center-me-v" center vertically only
//         ".util-ui-center-me"   center both vertically and horizontally
//     centering is done relative their parent
// IMPORTANT: will NOT center HIDDEN things
util.centerMes = function () {
    
    // horizontal centering
    $(".util-ui-center-me-h, .util-ui-center-me").each( function() {
        $(this).css({ "margin-left" : ( $(this).parent().width() - $(this).width() )/2 + "px" });
    });
    // vertical centering
    $(".util-ui-center-me-v, .util-ui-center-me").each( function() {
        $(this).css({ "margin-top" : ( $(this).parent().height() - $(this).height() )/2 + "px" });
    });
    
}; // centerMes
$(document).ready(  util.centerMes );
$(window  ).resize( util.centerMes );


//-----------------------------------------------------------
// getUrlVal
//     return "val" for input "name" for a "name=val" pair in the current page url
//     returns undefined if there is no name in the url
// IMPORTANT: both the input name and any name in url is treated case-insensitively
util.getUrlVal = function(name) {
    name = name.toLowerCase().replace(/[\[]/, "\\[").replace(/[\]]/, "\\]"); // make name  case insensitive
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)","i");               // make match case insensitive
    var results = regex.exec(location.search);
    return results === null ? undefined : decodeURIComponent(results[1].replace(/\+/g, " "));
}; // getUrlVal


//-----------------------------------------------------------
// cookies
//   cookie reader/writer framework with full unicode support
//
// method summary:
// * util.cookies.setItem( name, value[, end[, path[, domain[, secure]]]] )
// * util.cookies.getItem( name )
// * util.cookies.removeItem( name[, path] )
// * util.cookies.hasItem( name )
// * util.cookies.keys()
//
//----------------------------------------------------------
// WRITING A COOKIE
//    create/overwrite a cookie
//
// usage:
//    util.cookies.setItem( name, value[, end[, path[, domain[, secure]]]] )
//
// input:
//    name - required [string]
//       name of the cookie to create/overwrite
//
//    value - required [string]
//       value of the cookie
//
//    end - optional [number, Infinity, string, Date object, or null]
//       max-age in seconds (e.g. 31536e3 for a year, Infinity for a never-expires cookie)
//       or the expires date in GMTString format or as Date object
//       if not specified it is set to expire at the end of session
//
//    path - optional [string or null]
//       cookie path, eg: "/" or "/mydir"
//       if not specified, defaults to the current path of the current document location
//
//    domain - optional [string or null]
//       cookie dowmain, eg: "example.com", ".example.com" (includes all subdomains), or "subdomain.example.com"
//       if not specified, defaults to the host portion of the current document location
//
//    secure - optional [boolean or null]
//       whether to transmit the cookie only over secure protocol (https)
//
// examples:
//    util.cookies.setItem( "test0", "Hello world!" );
//    util.cookies.setItem( "test1", "Unicode test: \u00E0\u00E8\u00EC\u00F2\u00F9", Infinity );
//    util.cookies.setItem( "test2", "Hello world!", new Date(2020, 5, 12) );
//    util.cookies.setItem( "test3", "Hello world!", new Date(2027, 2, 3), "/blog" );
//    util.cookies.setItem( "test4", "Hello world!", "Sun, 06 Nov 2022 21:43:15 GMT" );
//    util.cookies.setItem( "test5", "Hello world!", "Tue, 06 Dec 2022 13:11:07 GMT", "/home" );
//    util.cookies.setItem( "test6", "Hello world!", 150 );
//    util.cookies.setItem( "test7", "Hello world!", 245, "/content" );
//    util.cookies.setItem( "test8", "Hello world!", null, null, "example.com" );
//    util.cookies.setItem( "test9", "Hello world!", null, null, null, true );
//
//----------------------------------------------------------
// GETTING A COOKIE
//    read a cookie
//    null is returned if the cookie does not exist
//
// usage:
//    util.cookies.getItem( name )
//
// input:
//    name - required [string]
//       name of the cookie to read
//
// examples:
//    alert( util.cookies.getItem("test1") );
//    alert( util.cookies.getItem("nonExistingCookie") );
//
//----------------------------------------------------------
// REMOVING A COOKIE
//    delete a cookie
//
// usage:
//    util.cookies.removeItem( name[, path] )
//
// input:
//    name - required [string]
//       the name of the cookie to remove
//
//    path - optional string or null]
//       cookie path, eg: "/" or "/mydir"
//       if not specified, defaults to the current path of the current document location
//
// examples:
//    util.cookies.removeItem( "test1" );
//    util.cookies.removeItem( "test5", "/home" );
//
//----------------------------------------------------------
// TESTING FOR A COOKIE
//    check if a cookie exists
//
// usage:
//    util.cookies.hasItem( name )
//
// input:
//    name (required)
//       the name of the cookie to test
//       (string)
//
// examples:
//    util.cookies.hasItem( "test1" );
//
//----------------------------------------------------------
// GETTING THE LIST OF ALL COOKIES
//    Returns an array of all readable cookies from this location
//
// usage:
//    util.cookies.keys()
//
// examples:
//    alert( util.cookies.keys().join("\n") ); 
//
//-----------------------------------------------------------
util.cookies = {
    
    setItem : function (sKey, sValue, vEnd, sPath, sDomain, bSecure) {
        if (!sKey || /^(?:expires|max\-age|path|domain|secure)$/i.test(sKey)) { return false; }
        var sExpires = "";
        if (vEnd) {
            switch (vEnd.constructor) {
                case Number : sExpires = vEnd === Infinity ? "; expires=Fri, 31 Dec 9999 23:59:59 GMT" : "; max-age=" + vEnd; break;
                case String : sExpires = "; expires=" + vEnd;                                                                 break;
                case Date   : sExpires = "; expires=" + vEnd.toGMTString();                                                   break;
            }
        }
        document.cookie = escape(sKey) + "=" + escape(sValue) + sExpires + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "") + (bSecure ? "; secure" : "");
        return true;
    },
    
    getItem : function (sKey) {
        return unescape(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + escape(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null;
    },
    
    removeItem : function (sKey, sPath) {
        if (!sKey || !this.hasItem(sKey)) { return false; }
        document.cookie = escape(sKey) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT" + (sPath ? "; path=" + sPath : "");
        return true;
    },
    
    hasItem : function (sKey) {
        return (new RegExp("(?:^|;\\s*)" + escape(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=")).test(document.cookie);
    },
    
    keys : function () {
        var aKeys = document.cookie.replace(/((?:^|\s*;)[^\=]+)(?=;|$)|^\s*|\s*(?:\=[^;]*)?(?:\1|$)/g, "").split(/\s*(?:\=[^;]*)?;\s*/);
        for (var nIdx = 0; nIdx < aKeys.length; nIdx++) { aKeys[nIdx] = unescape(aKeys[nIdx]); }
        return aKeys;
    }
    
};


//-----------------------------------------------------------
// slider - range slider
//   adapted from https://github.com/nitinhayaran/jRange (downloaded 6/19/2017)
//   original author: Nitin Hayaran (nitinhayaran@gmail.com), licensed under MIT
//
// creation:
//   $(selector).slider( {options} )
// methods:
//   $(selector).slider( 'method' [,options] )

// constructor
util.slider = function() {
    return this.init.apply(this, arguments);
};
util.slider.prototype = {
    
    //.............................
    // input options and defaults
    //.............................
    defaults: {
        
        // ..setup...
        from         : 0,             // [number ] lower bound of slider
        to           : 100,           // [number ] upper bound of slider
        step         : 1,             // [number ] slider step increment
        value        : 50,            // [number ] startup value, string of comma-separated values when range selector
        isRange      : false,         // [boolean] whether a double-pointer range selector, when true values are comma-seperated, eg: "25,75"
        width        : 300,           // [integer] width of the slider, px
        snap         : false,         // [boolean] whether to snap pointer to step values
        disable      : false,         // [boolean] whether to create disabled - see user methods for enabling/disabling after creation
        
        // ...annotation...
        showLabel    : false,         // [boolean] whether to show value label on top of slider pointer
        labelFormat  : "%s",          // formats label on pointer, either:
                                      //     [string  ] '%s' replaced by the value, eg: "%s days", "%s things", etc
                                      //     [function] labelFormat(value, pointer), return the label string for a custom label based on the value
        showScale    : false,         // [boolean] whether to show scale text below the slider
        scale        : undefined,     // [array  ] array containing labels which are shown in scale below the slider, eg: [0,25,50,100], default is [from,to]

        // ..events...
        onChange     : function() {}, // [function] executed whenever the value changes by user
        onDragEnd    : function() {}, // [function] executed when pointer drag ends, useful for doing something just once per slider drag
        onBarClicked : function() {}  // [function] executed when user clicks on the bar
    },
    
    //.............................
    // user methods
    //.............................
    
    // $(selector).slider( 'getValue' )
    //   return current slider value (string)
    //   if range slider, returns comma-separated values
    getValue: function() {
        return this.options.value;
    },

    // $(selector).slider( 'setValue', value )
    //   set current slider value (string)
    //   if range slider, input comma-separated values
    setValue: function(value) {
        var values = value.toString().split(",");
        values[0] = Math.min(Math.max(values[0], this.options.from), this.options.to) + "";
        if (values.length > 1){
            values[1] = Math.min(Math.max(values[1], this.options.from), this.options.to) + "";
        }
        this.options.value = value;
        var v = (values.length === 2 ? values : [0, values[0]]);
        var prc = [
            (parseFloat(v[0]) - parseFloat(this.options.from)) * 100 / this.interval,
            (parseFloat(v[1]) - parseFloat(this.options.from)) * 100 / this.interval
        ];
        if (this.isSingle()) {
            this.setPosition(this.hiPointer, prc[1]);
        } else {
            this.setPosition(this.loPointer, prc[0]);
            this.setPosition(this.hiPointer, prc[1]);
        }
    },

    // $(selector).slider( 'getOptions' )
    //   return current options object
    getOptions: function() {
        return this.options;
    },
    
    // $(selector).slider( 'setOptions', {options} )
    //   update option(s) after creation
    //   options not input keep their creation values
    setOptions: function(options) {
        var opts = this.options;
        $.each( options, function(k,v){
            opts[k] = v;
        });
        this.render();
    },

    // $(selector).slider( 'disable' )
    //   disable
    disable: function(){
        this.options.disable = true;
        this.setReadonly();
    },
    
    // $(selector).slider( 'enable' )
    //   enable
    enable: function(){
        this.options.disable = false;
        this.setReadonly();
    },
    
    // $(selector).slider( 'toggleDisable' )
    //   toggle enable-disabled state
    toggleDisable: function(){
        this.options.disable = !this.options.disable;
        this.setReadonly();
    },
    
    //.............................
    // private
    template:
        '<div class="slider-container">\
            <div class="back-bar">\
                <div class="selected-bar"></div>\
                <div class="pointer lo"></div><div class="pointer-label lo"></div>\
                <div class="pointer hi"></div><div class="pointer-label hi"></div>\
                <div class="clickable-bar"></div>\
            </div>\
            <div class="scale"></div>\
        </div>',
    init: function(node,options) {
        this.options       = $.extend({}, this.defaults, options);
        this.inputNode     = $(node);
        this.options.value = this.options.value || this.inputNode.val() || (this.options.isRange ? this.options.from + "," + this.options.from : "" + this.options.from);
        this.options.value = this.options.value.toString();
        this.domNode       = $(this.template);
        this.inputNode.after(this.domNode);
        this.domNode.on("change", this.onChange);
        this.pointers      = $(".pointer", this.domNode);
        this.loPointer     = this.pointers.first();
        this.hiPointer     = this.pointers.last();
        this.labels        = $(".pointer-label", this.domNode);
        this.loLabel       = this.labels.first();
        this.hiLabel       = this.labels.last();
        this.scale         = $(".scale", this.domNode);
        this.bar           = $(".selected-bar", this.domNode);
        this.clickableBar  = this.domNode.find(".clickable-bar");
        this.interval      = this.options.to - this.options.from;
        this.render();
    },
    render: function() {
        // set widget node
        if (this.inputNode.width() === 0 && !this.options.width) {
            console.warn("util [slider]: no width, doing nothing");
            return;
        }
        this.options.width = ( this.options.width || this.inputNode.width() );
        this.domNode.width(this.options.width);
        this.inputNode.hide();
        // show-hide things
        if (this.isSingle()) {
            this.loPointer.hide();
            this.loLabel.hide();
        } else {
            this.loPointer.show();
            this.loLabel.show();
        }
        if (!this.options.showLabel) {
            this.labels.hide();
        } else {
            this.labels.show();
        }
        if (this.options.showScale) {
            var s = this.options.scale || [this.options.from, this.options.to];
            var prc = Math.round((100 / (s.length - 1)) * 10) / 10;
            var str = "";
            for (var i = 0; i < s.length; i++) {
                str += '<span style="left: ' + (i*prc) + '%">' + (s[i] != '|' ? '<ins>' + s[i] + '</ins>' : '') + '</span>';
            }
            this.scale.html(str);
            $("ins", this.scale).each(function() {
                $(this).css({
                    marginLeft: -$(this).outerWidth() / 2
                });
            });
            this.scale.show();
        } else {
            this.scale.hide();
        }
        // attach events
        this.clickableBar.click($.proxy(this.barClicked, this));
        this.pointers.on("mousedown touchstart", $.proxy(this.onDragStart, this));
        this.pointers.bind("dragstart", function(event) {
            event.preventDefault();
        });
        // set value
        this.setValue(this.options.value);
    },
    barClicked: function(e) {
        if (this.options.disable) { return; }
        var x = e.pageX - this.clickableBar.offset().left;
        if (this.isSingle()) {
            this.setPosition(this.pointers.last(), x, true);
        } else {
            var firstLeft      = Math.abs(parseFloat(this.pointers.first().css("left"), 10));
            var firstHalfWidth = this.pointers.first().width() / 2;
            var lastLeft       = Math.abs(parseFloat(this.pointers.last().css("left"), 10));
            var lastHalfWidth  = this.pointers.first().width() / 2;
            var leftSide       = Math.abs(firstLeft - x + firstHalfWidth);
            var rightSide      = Math.abs(lastLeft  - x + lastHalfWidth );
            var pointer;
            if(leftSide == rightSide) {
                pointer = x < firstLeft ? this.pointers.first() : this.pointers.last();
            } else {
                pointer = leftSide < rightSide ? this.pointers.first() : this.pointers.last();
            }
            this.setPosition(pointer, x, true);
        }
        this.options.onBarClicked.call(this, this.options.value);
    },
    setPosition: function(pointer, position, isPx) {
        var loPos = parseFloat(this.loPointer.css("left"));
        var hiPos = parseFloat(this.hiPointer.css("left")) || 0;
        var circleWidth = this.hiPointer.width() / 2;
        if (!isPx) { position = (this.domNode.width() * position) / 100; }
        if(this.options.snap){
            var expPos = this.correctPositionForSnap(position);
            if(expPos === -1){
                return;
            } else {
                position = expPos;
            }
        }
        if (pointer[0] === this.hiPointer[0]) {
            hiPos = Math.round(position - circleWidth);
        } else {
            loPos = Math.round(position - circleWidth);
        }
        pointer.css({
            left : Math.round(position - circleWidth)
        });
        var leftPos = loPos + circleWidth;
        if (this.isSingle()) {
            leftPos = 0;
        }
        var w = Math.round(hiPos + circleWidth - leftPos);
        this.bar.css({
            width : Math.abs(w),
            left  : (w>0) ? leftPos : leftPos + w
        });
        this.showPointerValue(pointer, position);
        this.setReadonly();
    },
    correctPositionForSnap: function(position){
        var currentValue = this.positionToValue(position) - this.options.from;
        var diff = this.options.width / (this.interval / this.options.step);
        var expectedPosition = (currentValue / this.options.step) * diff;
        if( position <= expectedPosition + diff / 2 && position >= expectedPosition - diff / 2){
            return expectedPosition;
        }else{
            return -1;
        }
    },
    showPointerValue: function(pointer, position) {
        var label = $(".pointer-label", this.domNode)[pointer.hasClass("lo") ? "first" : "last"]();
        var text;
        var value = this.positionToValue(position);
        if ($.isFunction(this.options.labelFormat)) {
            var type = this.isSingle() ? undefined : (pointer.hasClass("lo") ? "lo" : "hi");
            text = this.options.labelFormat(value, type);
        } else {
            text = this.options.labelFormat.replace("%s", value);
        }
        var width = label.html(text).width();
        var left  = position - width / 2;
        left = Math.min(Math.max(left, 0), this.options.width - width);
        label.css({
            left: left
        });
        // set input value
        if (this.isSingle()) {
            this.options.value = value.toString();
        } else {
            var values = this.options.value.split(",");
            if (pointer.hasClass("lo")) {
                this.options.value = value + "," + values[1];
            } else {
                this.options.value = values[0] + "," + value;
            }
        }
        if (this.inputNode.val() !== this.options.value) {
            this.inputNode.val(this.options.value).trigger("change");
            this.options.onChange.call(this, this.options.value);
        }
    },
    positionToValue: function(pos) {
        var value = (pos / this.domNode.width()) * this.interval;
        value = parseFloat(value, 10) + parseFloat(this.options.from, 10);
        if ( ( this.options.value + this.options.from + this.options.to ).indexOf(".") > -1 ) {
            var final = Math.round(Math.round(value / this.options.step) * this.options.step *100)/100;
            if (final!==0.0) {
                final = "" + final;
                if (final.indexOf(".")===-1) {
                    final = final + ".";
                }
                while (final.length - final.indexOf(".")<3) {
                    final = final+"0";
                }
            } else {
                final = "0.00";
            }
            return final;
        } else {
            return Math.round(value / this.options.step) * this.options.step;
        }
    },

    isSingle: function() {
        if (typeof(this.options.value) === "number") { return true; }
        return (this.options.value.indexOf(",") !== -1 || this.options.isRange) ? false : true;
    },
    setReadonly: function(){
        this.domNode.toggleClass("slider-readonly", this.options.disable);
    },

    onDragStart: function(e) {
        if ( this.options.disable || (e.type === "mousedown" && e.which !== 1)) { return; }
        e.stopPropagation();
        e.preventDefault();
        var pointer = $(e.target);
        this.pointers.removeClass("last-active");
        pointer.addClass("focused last-active");
        this[(pointer.hasClass("lo") ? "lo" : "hi") + "Label"].addClass("focused");
        $(document).on("mousemove.slider touchmove.slider", $.proxy(this.onDrag, this, pointer));
        $(document).on("mouseup.slider touchend.slider touchcancel.slider", $.proxy(this.onDragEnd, this));
    },
    onDrag: function(pointer, e) {
        e.stopPropagation();
        e.preventDefault();
        if (e.originalEvent.touches && e.originalEvent.touches.length) {
            e = e.originalEvent.touches[0];
        } else if (e.originalEvent.changedTouches && e.originalEvent.changedTouches.length) {
            e = e.originalEvent.changedTouches[0];
        }
        var position = e.clientX - this.domNode.offset().left;
        this.domNode.trigger("change", [this, pointer, position]);
    },
    onDragEnd: function(e) {
        this.pointers.removeClass("focused").trigger("rangeslideend");
        this.labels.removeClass("focused");
        $(document).off(".slider");
        this.options.onDragEnd.call(this, this.options.value);
    },
    onChange: function(e, self, pointer, position) {
        var min = 0;
        var max = self.domNode.width();
        if (!self.isSingle()) {
            min = pointer.hasClass("hi") ? parseFloat(self.loPointer.css("left")) + (self.loPointer.width() / 2) : 0;
            max = pointer.hasClass("lo") ? parseFloat(self.hiPointer.css("left")) + (self.hiPointer.width() / 2) : self.domNode.width();
        }
        var value = Math.min(Math.max(position, min), max);
        self.setPosition(pointer, value, true);
    }
};

// jquery plugin wrapper
$.fn["slider"] = function(option) {
    var args = arguments;
    var result;
    this.each(function() {
        var $this = $(this),
            data = $.data(this, "plugin_"+"slider"),
            options = typeof option === "object" && option;
        if (!data) {
            $this.data("plugin_"+"slider", (data = new util.slider(this, options)));
            $(window).resize(function() {
                // update slider position to keep it synced with scale
                data.setValue(data.getValue());
            });
        }
        // if first argument is a string call as method, eg: $(.selector').plugin('function'[,args])
        if (typeof option === "string") {
            result = data[option].apply(data, Array.prototype.slice.call(args,1));
        }
    });
    return result || this;
};


//-----------------------------------------------------------
// toggle - bootstrap toggle switch
//   adapted from http://www.bootstraptoggle.com (downloaded 3/7/2018)
//   original author: Min Hur, The New York Times Company, licensed under MIT
//
// usage: see http://www.bootstraptoggle.com
+function ($) {

    //.............................
    // public
    //.............................

    var Toggle = function( element, options ){
        this.$element = $(element);
        this.options  = $.extend( {}, this.defaults(), options );
        this.render();
    };
    Toggle.VERSION  = "2.2.0";

    Toggle.DEFAULTS = {
        on       : "On",
        off      : "Off",
        onstyle  : "primary",
        offstyle : "default",
        size     : "normal",
        style    : "",
        width    : null,
        height   : null
    };

    Toggle.prototype.defaults = function(){
        return {
            on       : this.$element.attr("data-on"      ) || Toggle.DEFAULTS.on,
            off      : this.$element.attr("data-off"     ) || Toggle.DEFAULTS.off,
            onstyle  : this.$element.attr("data-onstyle" ) || Toggle.DEFAULTS.onstyle,
            offstyle : this.$element.attr("data-offstyle") || Toggle.DEFAULTS.offstyle,
            size     : this.$element.attr("data-size"    ) || Toggle.DEFAULTS.size,
            style    : this.$element.attr("data-style"   ) || Toggle.DEFAULTS.style,
            width    : this.$element.attr("data-width"   ) || Toggle.DEFAULTS.width,
            height   : this.$element.attr("data-height"  ) || Toggle.DEFAULTS.height
        };
    };

    Toggle.prototype.render = function(){
        this._onstyle  = "btn-" + this.options.onstyle;
        this._offstyle = "btn-" + this.options.offstyle;
        var size =
              this.options.size === "large" ? "btn-lg"
            : this.options.size === "small" ? "btn-sm"
            : this.options.size === "mini"  ? "btn-xs"
            : "";
        var $toggleOn     = $('<label class="btn">').html(this.options.on ).addClass(this._onstyle  + " " + size);
        var $toggleOff    = $('<label class="btn">').html(this.options.off).addClass(this._offstyle + " " + size + " active");
        var $toggleHandle = $('<span class="toggle-handle btn btn-default">').addClass(size);
        var $toggleGroup  = $('<div class="toggle-group">').append( $toggleOn, $toggleOff, $toggleHandle );
        var $toggle       = $('<div class="toggle btn" data-toggle="toggle">').addClass( this.$element.prop("checked") ? this._onstyle : this._offstyle+" off" ).addClass(size).addClass(this.options.style);

        this.$element.wrap($toggle);
        $.extend( this, {
            $toggle      : this.$element.parent(),
            $toggleOn    : $toggleOn,
            $toggleOff   : $toggleOff,
            $toggleGroup : $toggleGroup
        });
        this.$toggle.append( $toggleGroup );

        var width  = this.options.width  || Math.max( $toggleOn.outerWidth()  , $toggleOff.outerWidth()  ) + ( $toggleHandle.outerWidth()/2 );
        var height = this.options.height || Math.max( $toggleOn.outerHeight() , $toggleOff.outerHeight() );
        $toggleOn.addClass("toggle-on");
        $toggleOff.addClass("toggle-off");
        this.$toggle.css({ width:width, height:height });
        if (this.options.height){
            $toggleOn.css(  "line-height" , $toggleOn.height()  + "px" );
            $toggleOff.css( "line-height" , $toggleOff.height() + "px" );
        }
        this.update(true);
        this.trigger(true);
    };

    Toggle.prototype.toggle = function(){
        if (this.$element.prop("checked")){
            this.off();
        } else {
            this.on();
        }
    };

    Toggle.prototype.on = function(silent){
        if (this.$element.prop("disabled")){ return false; }
        this.$toggle.removeClass(this._offstyle + " off").addClass(this._onstyle);
        this.$element.prop("checked", true);
        if (!silent){ this.trigger(); }
    };

    Toggle.prototype.off = function(silent){
        if (this.$element.prop("disabled")){ return false; }
        this.$toggle.removeClass(this._onstyle).addClass(this._offstyle + " off");
        this.$element.prop("checked", false);
        if (!silent){ this.trigger(); }
    };

    Toggle.prototype.enable = function(){
        this.$toggle.removeAttr("disabled");
        this.$element.prop("disabled", false);
    };

    Toggle.prototype.disable = function(){
        this.$toggle.attr("disabled", "disabled");
        this.$element.prop("disabled", true);
    };

    Toggle.prototype.update = function(silent){
        if (this.$element.prop("disabled")){
            this.disable();
        } else {
            this.enable();
        }
        if (this.$element.prop("checked")){
            this.on(silent);
        } else {
            this.off(silent);
        }
    };

    Toggle.prototype.trigger = function(silent) {
        this.$element.off("change.bs.toggle");
        if (!silent){ this.$element.change(); }
        this.$element.on("change.bs.toggle", $.proxy( function(){
            this.update();
        }, this) );
    };

    Toggle.prototype.destroy = function(){
        this.$element.off("change.bs.toggle");
        this.$toggleGroup.remove();
        this.$element.removeData("bs.toggle");
        this.$element.unwrap();
    };

    //.............................
    // plugin
    //.............................

    function Plugin(option){
        return this.each( function(){
            var $this   = $(this);
            var data    = $this.data("bs.toggle");
            var options = typeof option == "object" && option;
            if (!data){ $this.data("bs.toggle", (data = new Toggle(this, options))); }
            if (typeof option == "string" && data[option]){ data[option](); }
        });
    }
    var old = $.fn.bootstrapToggle;
    $.fn.bootstrapToggle             = Plugin;
    $.fn.bootstrapToggle.Constructor = Toggle;

    //.............................
    // no conflict
    //.............................

    $.fn.toggle.noConflict = function(){
        $.fn.bootstrapToggle = old;
        return this;
    };

    //.............................
    // data api
    //.............................

    $( function(){
        $("input[type=checkbox][data-toggle^=toggle]").bootstrapToggle();
    });
    $(document).on( "click.bs.toggle", "div[data-toggle^=toggle]", function(e){
        var $checkbox = $(this).find("input[type=checkbox]");
        $checkbox.bootstrapToggle("toggle");
        e.preventDefault();
    });

}(jQuery);


//===========================================================
// END
//===========================================================
console.log("[util_ui] loaded and ready");

